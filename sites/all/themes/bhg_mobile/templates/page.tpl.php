<?php
// $Id: page.tpl.php,v 1.1.2.1 2010/06/17 07:54:57 sociotech Exp $
?>
<!DOCTYPE html>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
<head>
<title><?php print $head_title; ?></title>
<meta name="viewport" content="height=device-height, width=device-width, minimum-scale=1.0, user-scalable=0" />
<?php print $head; ?><?php print $styles; ?><?php print $setting_styles; ?><?php print $local_styles; ?>
<script type="text/javascript" src="http://use.typekit.com/sih4kgy.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<?php print $scripts; ?>
</head>

<body id="<?php print $body_id; ?>" class="normal <?php print $body_classes; ?>">
  <div id="header" class="clearfix <?php print $grid_width; ?>">
<?php if ($logo): ?>
    <div id="logo"><a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>"><img src="/sites/all/themes/bhg_mobile/images/bg/bhg_mobile_logo_back.png" alt="<?php print t('Home'); ?>" /></a></div>
<?php endif; ?>
  </div><!-- /header -->

  <div id="page">
    <div id="main" class="main row clearfix <?php print $grid_width; ?>">
      <div id="main-group" class="main-group row nested <?php print $main_group_width; ?>">
        <div id="content-group" class="content-group row nested <?php print $content_group_width; ?>"><?php print theme('grid_block', $breadcrumb, 'breadcrumbs'); ?><?php print theme('grid_block', $help, 'content-help'); ?><?php print theme('grid_block', $messages, 'content-messages'); ?><a name="main-content-area" id="main-content-area"></a><?php print theme('grid_block', $tabs, 'content-tabs'); ?>
          <div id="content-inner" class="content-inner block">
            <div id="content-inner-inner" class="content-inner-inner inner">
<?php if ($title): ?>
              <h1 class="title"><?php print $title; ?></h1>
<?php endif; ?>
<?php if ($content): ?>
              <div id="content-content" class="content-content"><?php print $content; ?></div><!-- /content-content -->
<?php endif; ?>
            </div><!-- /content-inner-inner --> 
          </div><!-- /content-inner --> 
        </div><!-- /content-group --> 
      </div><!-- /main-group --> 
    </div><!-- /main --> 
  </div><!-- /page -->

<?php if ($footer): ?>
   <div id="footer" class="clearfix <?php print $grid_width; ?>"><?php print $footer; ?></div>
<?php endif; ?>
<?php print $closure; ?>
</body>
</html>
