<?php
// $Id: page.tpl.php,v 1.1.2.1 2010/06/17 07:54:57 sociotech Exp $
?>
<!DOCTYPE html>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
<head>
<title><?php print $head_title; ?></title>
<meta name="viewport" content="height=device-height, width=device-width, minimum-scale=1.0, user-scalable=0" />
<?php print $head; ?><?php print $styles; ?><?php print $setting_styles; ?><?php print $local_styles; ?><?php print $scripts; ?>
</head>

<body<?php if (!empty($body_id)) { print ' id="'.$body_id.'"'; } ?> class="<?php print $body_classes; ?>">

  <div id="header" class="<?php print $grid_width; ?>">
<?php if ($logo): ?>
    <div id="logo"><a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a></div>
<?php endif; ?>
  </div><!-- /header -->

  <div id="page">
    <div id="home" class="home row clearfix <?php print $grid_width; ?>">
<?php if (!empty($home)): ?>
      <div id="homecontent"><?php print $home; ?></div><!-- /homecontent -->
<?php endif; ?>
    </div><!-- /home --> 
  </div><!-- /page -->

<?php if ($footer): ?>
  <div id="footer" class="<?php print $grid_width; ?>"><?php print $footer; ?></div><!-- /footer -->
<?php endif; ?>

<?php print $closure; ?>
</body>
</html>
