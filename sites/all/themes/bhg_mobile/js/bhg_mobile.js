// JavaScript Document
jQuery(document).ready(function(){
	// solutions finder node form
	var $_GET = {};
	document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
		function decode(s) {
			return decodeURIComponent(s.split("+").join(" "));
		}
		$_GET[decode(arguments[1])] = decode(arguments[2]);
	});
	
	// webform radio submits
	$('#webform-client-form-2 #webform-component-step-3--how-would-you-like-to-contact-us .form-radios .form-item .form-radio')
		.removeAttr('checked')
		.change(function() {
			if ('edit-submitted-step-3-how-would-you-like-to-contact-us-3' !== $(this).attr('id')) {
				$(this).parents('form').submit();
			}

		});
	
	// home page slider
	$('#block-views-nodequeue_2-block_1 #views_slideshow_singleframe_teaser_section_nodequeue_2-block_1').width($(window).width());
	$(window).resize(function() {
		$('#block-views-nodequeue_2-block_1 #views_slideshow_singleframe_teaser_section_nodequeue_2-block_1').width($(window).width());
	});
	
	$('ul.pager .pager-form select').change(function () {
		var $i = $(this),
			$form = $i.closest('form'),
			$q = $form.find('input[name="q"]').val(),
			$p = $i.val();
		window.location.href = '?q='+$q+'&page='+$p;
	});
	
	if ($('#edit-submitted-date').length > 0) {
		$('#edit-submitted-date').datepicker({
			showOn: 'button',
			buttonImage: '/sites/all/themes/bhg/images/btn/calendar.png',
			buttonImageOnly: true,
			dateFormat: 'mm/dd/yy',
			minDate: 0
		});
	}

	// support page, show loan number
	$('#webform-client-form-2 #webform-component-step-1--i-need-assistance-with .form-item label').click(function() {
		$('#webform-component-step-2 > .webform-component-select').hide().find('.js-check').removeClass('js-check');
		if ($(this).find('input').val() == 'application') {
			$('#webform-client-form-2 #webform-component-step-1--please-enter-your-application-loan-number').show();
			$('#webform-component-step-2 > div#webform-component-step-2--inquiry-application').show().find('select').addClass('js-check');
		} else if ($(this).find('input').val() == 'loan') {
			$('#webform-client-form-2 #webform-component-step-1--please-enter-your-application-loan-number').show();
			$('#webform-component-step-2 > div#webform-component-step-2--inquiry-myloan').show().find('select').addClass('js-check');
		}	else {
			$('#webform-client-form-2 #webform-component-step-1--please-enter-your-application-loan-number').hide();
			$('#webform-component-step-2 > div#webform-component-step-2--inquiry-customerservice').show().find('select').addClass('js-check');
		}
	});
	$('#webform-client-form-2 #webform-component-step-3--how-would-you-like-to-contact-us label').click(function() {
		/*if (!$('#edit-submitted-step-1-please-enter-your-application-loan-number').is(':hidden') && $('#edit-submitted-step-1-please-enter-your-application-loan-number').val().length < 1) {
			alert('Please enter your application/loan number');
			return false;
		}*/
		if ($('#webform-component-step-2 .js-check :selected').val().length < 1) {
			alert('Tell us more about your inquiry is mandatory');
			return false;
		}
		return true;
	});

	$('#node-form.affiliate-partnership .form-item, #node-form.loan-activation .form-item').each(function (i, t) {
		if (0 === (i+1)%2) {
			$(t).addClass('right');
		}
	});
	$('#node-form .node-form .standard').addClass('clearfix');

        $('form.webform-client-form, form#node-form').find('.form-item')        
                .find('.error')                                                 
                        .filter('select')                                       
                                .removeClass('error')                           
                                .closest('.selector').addClass('error').end()   
                        .end()                                                  
                        .filter('input[type="radio"]')                          
                                .removeClass('error')                           
                                .closest('.form-radios').addClass('error').end()
                        .end()                                                  
                .end()                                                          
                .find('.error')                                                 
                        .filter(function () {                                   
                                return !$(this).siblings('.validated').length;  
                        })                                                      
                        .after('<div class="validated not-check">')             
        ;                                                                       

        $('form.webform-client-form, form#node-form')                                                                        
                .find('select')                                                                                              
                .not('#node-form .step1 #edit-field-solution-profession-value, #edit-submitted-time, #edit-submitted-time-of-day, #edit-submitted-phone-type')
                .change(function() {                                                                          
                var $me = $(this),                                                                                 
                        $form_item = $me.parents('.form-item'),                                                             
                        $validated;                                                                                          
                if ($('.validated', $form_item).length < 1) {                                                                
                        $form_item.find('.selector').after('<div class="validated"> </div>');                                
                }                                                                                                            
                $validated = $form_item.find('.validated');                                                                  
                if ($me.is('.required, #webform-client-form-2 #webform-component-step-2 select')) {                          
                        if ($me.val() == '') {                                                                               
                                $validated.addClass('not-check').removeClass('check'); 
                                return true; 
                        } else { 
                                $validated.addClass('check').removeClass('not-check');                                       
                        } 
                }
        });

        $('form.webform-client-form, form#node-form').find('input[type="text"], input[type="email"], input.email, textarea')
                .not('#edit-submitted-phone-1,  #edit-submitted-phone-2, #edit-submitted-phone-3')
                .change(function() {
                        var $me = $(this),
                                $form_item = $me.parents('.form-item'),
                                $form_el = $me,
                                $validated,
                                emailrx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                                daterx = /\d{2}\/\d{2}\/\d{4}/;
                        if ($('.validated', $form_item).length < 1) {
                                if ($me.parents('.resizable-textarea').length > 0) {
                                        $form_el = $me.closest('.resizable-textarea');
                                }
                                $form_el.after('<div class="validated"> </div>');
                        }
                        $validated = $form_item.find('.validated');
                        if ('' === $me.val()) {
                                if ($me.is('.required')) {
                                        $validated.addClass('not-check').removeClass('check');
                                        return true;
                                } else {
                                        $validated.remove();
                                }
                                return true;
                        }
                        if ($me.is('.email, [type="email"], , #edit-submitted-newsletteremail-newsletter-email-address, #edit-field-solution-email-0-email')) {
                                if (!emailrx.test($me.val())) {
                                        $validated.addClass('not-check').removeClass('check');
                                        return true;
                                }
                        }
                        if ($me.is('#edit-submitted-verify-email')) {
                                if ($me.val() !== $('#edit-submitted-newsletteremail-newsletter-email-address').val()) {
                                        $validated.addClass('not-check').removeClass('check');
                                        return true;
                                }
                        }
                        if ($me.is('#edit-submitted-date')) {
                                if (!daterx.test($me.val())) {
                                        $validated.addClass('not-check').removeClass('check');
                                        return true;
                                }
                        }
                        $validated.addClass('check').removeClass('not-check');
                });
        $('#edit-submitted-phone-1,  #edit-submitted-phone-2, #edit-submitted-phone-3').change(function () {
                var $me = $(this),
                        $form_item = $me.parents('.webform-component-textfield').siblings('#webform-component-phone-type').find('.form-item'),
                        $form_el = $form_item.find('.selector'),
                        $validated;
                if ($('.validated', $form_item).length < 1) {
                        $form_el.after('<div class="validated"> </div>');
                }
                $validated = $form_item.find('.validated');
                if ('' === $me.val()) {
                        $validated.addClass('not-check').removeClass('check');
                        return true;
                }
                if ($me.is('#edit-submitted-phone-1, #edit-submitted-phone-2')) {
                        if (!/\d{3}/.test($me.val())) {
                                $validated.addClass('not-check').removeClass('check');
                                return true;
                        }
                }
                if ($me.is('#edit-submitted-phone-3')) {
                        if (!/\d{4}/.test($me.val())) {
                                $validated.addClass('not-check').removeClass('check');
                                return true;
                        }
                }
                if ((!/\d{3}/.test($('#edit-submitted-phone-1').val()))
                        || (!/\d{3}/.test($('#edit-submitted-phone-2').val()))
                        || (!/\d{4}/.test($('#edit-submitted-phone-3').val()))) {
                        return true;
                }
                $validated.addClass('check').removeClass('not-check');
        });

	// add 'date' to request-call form and pop up calendar
	$('#webform-client-form-3 input#edit-submitted-date').val('Date');
	$('#webform-client-form-3 input#edit-submitted-date').click(function() {
		$('#webform-client-form-3 .ui-datepicker-trigger').trigger('click');
	});
	// create custom form elements
	$('option').each(function() {
		$(this).text($(this).text().replace('- Any -', $(this).parents('.views-exposed-widget').children('label').eq(0).text()));
	});
	
	// Add Uniform to select boxes
	$('select').removeAttr('multiple').uniform();

	// Update email function on social buttons
	$('.mailbtn').click(function() {
		return addthis_sendto('email');
	});
	
});
