String.prototype.toUpper = function(){
	return this.replace(/^([a-z])|([\s+\-][a-z])/g, function($1){return $1.toUpperCase().replace('-',' ');});
};

// JavaScript Document
jQuery(document).ready(function(){
	$('a.active-trail').parents('li').addClass('active-trail');
	jQuery('#book').fadeIn("slow");
	jQuery('#block-views-work-block img').hover(function() {
		var title = jQuery(this).attr('title');
		jQuery('#block-views-work-block').append('<div class="title">' + title + '</div>');
		// TODO: do something with the title
	}, function() {
		jQuery('.title').remove();
		// this will be triggered when the mouse pointer leaves the element
	});

	jQuery('#block-views-work-block a')
		.click(false)
		.hover(function() {
			var pic = jQuery(this).attr('href');
			if(pic != "javascript:void(0)") {
				jQuery('#main').append('<div class="picture"><img src="' + pic + '" /></div>');
			}
			// TODO: do something with the title
		}, function() {
			jQuery('.picture').remove();
			// this will be triggered when the mouse pointer leaves the element
		});

	// solutions finder node form
	var $_GET = {};
	document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
		function decode(s) {
			return decodeURIComponent(s.split("+").join(" "));
		}
		$_GET[decode(arguments[1])] = decode(arguments[2]);
	});
//Unnecessary since we moved to uniform from jquery dropdown
/*
	if ($_GET['q'] == 'node/add/solution-finder') {
		$('#node-form').find('.jquery_dropdown_list a').click(function() {
			$(this).parents('.jquery_dropdown_container').siblings('.check').remove();
			if ($(this).parents('.form-item').find('select').val() > 0) { $(this).parents('.jquery_dropdown_container').after('<div class=\"check\">check</div>'); }
		});
	}
*/
	$('.view.view-media-newsroom .views-row .award .award-content')
		.find('p').hide().end()
		.hover(function() {
			$(this)
				.find('p').show().end()
				.height(115);
		}, function() {
			$(this)
				.find('p').hide().end()
				.height(34);
		}
	);

	$('#quicktabs-home_block .hometab').hover(function() {
		var $c = $(this).find('.home-content');
		'undefined' === typeof $c.data('height') && $c.data('height', $c.css('height'));
		$c.stop(true).animate({height:115})
		.find('.truncate_ellipsis').hide().end()
		.find('.truncate_more').show().end();
	}, function() {
		var $c = $(this).find('.home-content');
		'undefined' === typeof $c.data('height') && $c.data('height', '34px');
		$c.stop(true).animate({height:$c.data('height')})
			.find('.truncate_ellipsis').show().end()
			.find('.truncate_more').hide().end();
	});

	// webform radio submits
	$('#webform-client-form-2 #webform-component-step-3--how-would-you-like-to-contact-us .form-radios .form-item .form-radio')
		.removeAttr('checked')
		.change(function() {
			if ('edit-submitted-step-3-how-would-you-like-to-contact-us-3' !== $(this).attr('id')) {
				$(this).parents('form').submit();
			}

		});

	// home page slider
	$('body.front #block-views-nodequeue_2-block').prependTo('body.front #page');
	$('body.front #block-views-nodequeue_2-block #views_slideshow_singleframe_main_nodequeue_2-block').width($(window).width());
	$(window).resize(function() {
		$('body.front #block-views-nodequeue_2-block #views_slideshow_singleframe_main_nodequeue_2-block').width($(window).width());
	});
	// filter with calendar click
	$('#views-exposed-form-media-newsroom-page-1 .container-inline-date').click(function() {
		$(this).find('input').trigger('focus').trigger('click');
	});
	$('#views-exposed-form-media-newsroom-page-2 .container-inline-date').click(function() {
		$(this).find('input').trigger('focus').trigger('click');
	});
	$('#views-exposed-form-media-newsroom-page-3 .container-inline-date').click(function() {
		$(this).find('input').trigger('focus').trigger('click');
	});
	$('#views-exposed-form-media-newsroom-page-4 .container-inline-date').click(function() {
		$(this).find('input').trigger('focus').trigger('click');
	});

	// header search dropdown
	$('#views-exposed-form-search-default .jquery_dropdown_header').click(function() {
		$('#views-exposed-form-search-default .jquery_dropdown_list').toggle();
	});

	// modal iframe, remove _new target
	$('body > div.modalframe-page-wrapper .view-id-executive_bios.view-display-id-page_2 ul.pager a').removeAttr('target');

	$('ul.pager .pager-form select').change(function () {
		var $i = $(this),
			$form = $i.closest('form'),
			$q = $form.find('input[name="q"]').val(),
			$p = $i.val();
		window.location.href = '?q='+$q+'&page='+$p;
	});

	if (0 < $('#edit-submitted-date').length) {
		$('#edit-submitted-date').datepicker({
			showOn: 'button',
			buttonImage: '/sites/all/themes/bhg/images/btn/calendar.png',
			buttonImageOnly: true,
			dateFormat: 'mm/dd/yy',
			minDate: 0
		});
	}

	$('#edit-submit-media-newsroom').show();

	// Awards click
	$('.view-media-newsroom.view-id-media_newsroom.view-display-id-page_4 div.views-row').click(function() {
		var href = $(this).find("a").attr("href");
		if(href) {
			window.location = href;
		}
	});

	$('#quicktabs_container_home_block .home-content p .jtruncate')
		.jTruncate({
			length: 41,
			minTrail: 0,
			moreText: "",
			lessText: "",
			ellipsisText: "..."
		})
		.find('div.clearboth').remove();

	//Add search field watermark
	$('#edit-keys').watermark("enter search term");
	if ('undefined' !== typeof $_GET['q']) {
		var args = $_GET['q'].split('/'),
			wm = 'Search ';
		switch (args[0]) {
			case 'tips-resources':
				wm += args[1].toUpper();
				break;
			case 'company':
				wm += args[1].toUpper();
				break;
			default:
		}
	}
	($('#edit-title').length > 0) && $('#edit-title').watermark(wm);
	$('#edit-submitted-date, #edit-field-date-value-min-datepicker-popup-0, #edit-field-date-value-max-datepicker-popup-0').watermark("mm/dd/yyyy");

	// support page, show loan number
	$('#webform-client-form-2 #webform-component-step-1--i-need-assistance-with .form-item label').click(function() {
		$('#webform-component-step-2 > .webform-component-select').hide().find('.js-check').removeClass('js-check');
		if ($(this).find('input').val() == 'application') {
			$('#webform-client-form-2 #webform-component-step-1--please-enter-your-application-loan-number').show();
			$('#webform-component-step-2 > div#webform-component-step-2--inquiry-application').show().find('select').addClass('js-check');
		} else if ($(this).find('input').val() == 'loan') {
			$('#webform-client-form-2 #webform-component-step-1--please-enter-your-application-loan-number').show();
			$('#webform-component-step-2 > div#webform-component-step-2--inquiry-myloan').show().find('select').addClass('js-check');
		}	else {
			$('#webform-client-form-2 #webform-component-step-1--please-enter-your-application-loan-number').hide();
			$('#webform-component-step-2 > div#webform-component-step-2--inquiry-customerservice').show().find('select').addClass('js-check');
		}
	});
	$('#webform-client-form-2 #webform-component-step-3--how-would-you-like-to-contact-us label').click(function() {
		/*if (!$('#edit-submitted-step-1-please-enter-your-application-loan-number').is(':hidden') && $('#edit-submitted-step-1-please-enter-your-application-loan-number').val().length < 1) {
			alert('Please enter your application/loan number');
			return false;
		}*/
		if ($('#webform-component-step-2 .js-check :selected').val().length < 1) {
			alert('Tell us more about your inquiry is mandatory');
			return false;
		}
		return true;
	});
/*
	$('#node-form.affiliate-partnership .form-item, #node-form.loan-activation .form-item').each(function (i, t) {
		if (0 === (i+1)%2) {
			$(t).addClass('right');
		}
	});
*/

	$('#node-form .node-form .standard').addClass('clearfix');

	// add 'date' to request-call form and pop up calendar
	$('#webform-client-form-3 input#edit-submitted-date')
		.click(function() {
			$('#webform-client-form-3 .ui-datepicker-trigger').trigger('click');
		}
	);

	// create custom form elements
	$('option').each(function() {
		$(this).text($(this).text().replace('- Any -', $(this).parents('.views-exposed-widget').children('label').eq(0).text()));
	});

	// Add Uniform to select boxes
	$('select').removeAttr('multiple').uniform();

	$('form.webform-client-form, form#node-form').find('.form-item')
		.find('.error')
			.filter('select')
				.removeClass('error')
				.closest('.selector').addClass('error').end()
			.end()
			.filter('input[type="radio"]')
				.removeClass('error')
				.closest('.form-radios').addClass('error').end()
			.end()
		.end()
		.find('.error')
			.filter(function () {
				return !$(this).siblings('.validated').length;
			})
			.after('<div class="validated not-check">')
	;

	// Update email function on social buttons
	$('.mailbtn').click(function() {
		return addthis_sendto('email');
	});

	$('form.webform-client-form, form#node-form')
		.find('select')
		.not('#node-form .step1 #edit-field-solution-profession-value, #edit-submitted-time, #edit-submitted-time-of-day, #edit-submitted-phone-type')
		.change(function() {
		var $me = $(this),
			$form_item = $me.parents('.form-item'),
			$validated;
		if ($('.validated', $form_item).length < 1) {
			$form_item.find('.selector').after('<div class="validated"> </div>');
		}
		$validated = $form_item.find('.validated');
		if ($me.is('.required, #webform-client-form-2 #webform-component-step-2 select')) {
			if ($me.val() == '') {
				$validated.addClass('not-check').removeClass('check');
				return true;
			} else {
				$validated.addClass('check').removeClass('not-check');
			}
		}
	});

	// Validate email address on Solution Finder
	// EDIT: How bout we use this for all e-mails??? :-D
	// ANOTHER EDIT: This is going to be our text field validator
	$('form.webform-client-form, form#node-form').find('input[type="text"], input[type="email"], input.email, textarea')
		.not('#edit-submitted-phone-1,  #edit-submitted-phone-2, #edit-submitted-phone-3')
		.change(function() {
			var $me = $(this),
				$form_item = $me.parents('.form-item'),
				$form_el = $me,
				$validated,
				emailrx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
				daterx = /\d{2}\/\d{2}\/\d{4}/;
			if ($('.validated', $form_item).length < 1) {
				if ($me.parents('.resizable-textarea').length > 0) {
					$form_el = $me.closest('.resizable-textarea');
				}
				$form_el.after('<div class="validated"> </div>');
			}
			$validated = $form_item.find('.validated');
			if ('' === $me.val()) {
				if ($me.is('.required')) {
					$validated.addClass('not-check').removeClass('check');
					return true;
				} else {
					$validated.remove();
				}
				return true;
			}
			if ($me.is('.email, [type="email"], , #edit-submitted-newsletteremail-newsletter-email-address, #edit-field-solution-email-0-email')) {
				if (!emailrx.test($me.val())) {
					$validated.addClass('not-check').removeClass('check');
					return true;
				}
			}
			if ($me.is('#edit-submitted-verify-email')) {
				if ($me.val() !== $('#edit-submitted-newsletteremail-newsletter-email-address').val()) {
					$validated.addClass('not-check').removeClass('check');
					return true;
				}
			}
			if ($me.is('#edit-submitted-date')) {
				if (!daterx.test($me.val())) {
					$validated.addClass('not-check').removeClass('check');
					return true;
				}
			}
			$validated.addClass('check').removeClass('not-check');
		});
	$('#edit-submitted-phone-1,  #edit-submitted-phone-2, #edit-submitted-phone-3').change(function () {
		var $me = $(this),
			$form_item = $me.parents('.webform-component-textfield').siblings('#webform-component-phone-type').find('.form-item'),
			$form_el = $form_item.find('.selector'),
			$validated;
		if ($('.validated', $form_item).length < 1) {
			$form_el.after('<div class="validated"> </div>');
		}
		$validated = $form_item.find('.validated');
		if ('' === $me.val()) {
			$validated.addClass('not-check').removeClass('check');
			return true;
		}
		if ($me.is('#edit-submitted-phone-1, #edit-submitted-phone-2')) {
			if (!/\d{3}/.test($me.val())) {
				$validated.addClass('not-check').removeClass('check');
				return true;
			}
		}
		if ($me.is('#edit-submitted-phone-3')) {
			if (!/\d{4}/.test($me.val())) {
				$validated.addClass('not-check').removeClass('check');
				return true;
			}
		}
		if ((!/\d{3}/.test($('#edit-submitted-phone-1').val())) 
			|| (!/\d{3}/.test($('#edit-submitted-phone-2').val())) 
			|| (!/\d{4}/.test($('#edit-submitted-phone-3').val()))) {
			return true;
		}
		$validated.addClass('check').removeClass('not-check');
	});
/*

	$('#webform-client-form-3 #edit-submitted-date').change(function () {
		var $me = $(this),
			$form_item = $me.parents('.form-item'),
			$form_el = $me,
			$validated;
		if ($('.validated', $form_item).length < 1) {
			if ($me.parents('#webform-component-date').length > 0) {
				$form_el = $me.closest('#webform-component-date');
			}
			$form_el.after('<div class="validated"> </div>');
		}
		$validated = $form_item.find('.validated');
	console.log($(this).val());

		}
	});
*/
/* Pretty sure this isn't necessary since this was a Drupal back-end bug
	// Modify 'BHG-Advantage' header link to account for weird IE8 bug
	$('div.menu-minipanel.menu-minipanel-611').live('load', function() {
		$('.menu-minipanel.menu-minipanel-611').unbind();
		$('#block-menu_block-1 .active-trail').removeClass('active-trail');
		$('.menu-minipanel.menu-minipanel-611').hover(function(e) {
			e.stopImmediatePropagation();
			$('div[qtip="0"]').show();
		}, function() {
			e.stopImmediatePropagation();
			$('div[qtip="0"]').hide();
		});
	});
*/
});
