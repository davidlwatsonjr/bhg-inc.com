<?php

/**
 * @file
 * Default theme implementation to format an HTML mail.
 *
 * Copy this file in your default theme folder to create a custom themed mail.
 * Rename it to mimemail-message--[mailkey].tpl.php to override it for a
 * specific mail.
 *
 * Available variables:
 * - $subject: The message subject.
 * - $body: The message body in HTML format.
 * - $mailkey: The message identifier.
 * - $recipient: An email address or user object who is receiving the message.
 * - $css: Internal style sheets.
 * - $assets: Fully-qualified path to images and stylesheets
 *
 * @see template_preprocess_mimemail_message()
 */

 $assets = "http://bhg-inc.com.staging.mindcomet.net/sites/all/themes/bhg/";
 $body = "";
 //$recipient = "amber@mindcomet.com, jim@mindcomet.com";
?>
<?php if ($mailkey == "mail-loan-activation-to-user") { ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css" media="screen">
.ReadMsgBody { width: 100%; }
.ExternalClass { width: 100%; display: block !important; }
#mpf0_MsgContainer{ display: block !important; }
v\:* { behavior: url(#default#VML); display:inline-block; }
body { width:100% !important; font-family:tahoma, arial, helvetica, sans-serif; font-size:11px; color:#474747; background-color:#ffffff; }
table td { border-collapse: collapse; }
a { font-family:tahoma, arial, helvetica, sans-serif; text-decoration:none; font-size:11px; color:#ffffff; }
</style>
</head>

<body topmargin="0" style="margin-top: 0px;" bgcolor="#ffffff" id="mimemail-body" <?php if ($mailkey): print 'class="'. $mailkey .'"'; endif; ?>>
<div align="center">
<center>
<table width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
		<tr>
			<td valign="top" align="center">
			<table width="700" bgcolor="#ffffff" cellspacing="0" cellpadding="0" align="center">
				
				<tr>
					<td width="700" align="center" colspan="3"><br><center><span style="font-family: Arial, Helvetica, sans-serif; color: #7a7a7a; font-size: 11px; line-height: 14px; text-decoration: none;">Note: To ensure delivery to your inbox, please add <a href="mailto:info@bhg-inc.com" style="font-family: Arial, Helvetica, sans-serif; color: #005696; font-size: 11px; line-height: 14px; text-decoration: underline;"><span style="font-family: Arial, Helvetica, sans-serif; color: #005696; font-size: 11px; line-height: 14px; text-decoration: underline;">info@bhg-inc.com</span></a> to your address book.</span></center><br></td>
				</tr>
			</table>
			
			<table width="661" bgcolor="#ffffff" cellspacing="0" cellpadding="0" align="center">
				<tr height="66">
					<td width="359" colspan="2">
						<a href="http://www.bhg-inc.com"><img src="<?php echo $assets; ?>images/newsletter/bhg-header-left.jpg" width="359" height="66"></a>
					</td>
					<td width="302" colspan="3">
						<img src="<?php echo $assets; ?>images/newsletter/bhg-header-right.jpg" width="302" height="66">
					</td>
				</tr>

				<tr height="273">
					<td width="359" colspan="2">
						<img src="<?php echo $assets; ?>images/newsletter/bhg-image-left.jpg" width="359" height="273">
					</td>

					<td width="302" colspan="3">
						<img src="<?php echo $assets; ?>images/newsletter/bhg-image-right.jpg" width="302" height="273">
					</td>
				</tr>

				<tr height="106">
					<td width="359" colspan="2">
						<img src="<?php echo $assets; ?>images/newsletter/bhg-content-left-top.jpg" width="359" height="106">
					</td>

					<td width="302" colspan="3">
						<a href="http://bhg-inc.com.staging.mindcomet.net/?q=bhg-advantage/customer-testimonials"><img src="<?php echo $assets; ?>images/newsletter/bhg-content-right-top.jpg" width="302" height="106"></a>
					</td>
				</tr>

				<tr height="60">
					<td width="359" colspan="2">
						<img src="<?php echo $assets; ?>images/newsletter/bhg-content-left-bottom.jpg" width="359" height="60">
					</td>

					<td width="359" colspan="3">
						<a href="http://bhg-inc.com.staging.mindcomet.net/?q=faq"><img src="<?php echo $assets; ?>images/newsletter/bhg-content-right-bottom.jpg" width="302" height="60"></a>
					</td>
				</tr>

				<tr height="56">
					<td width="106">
						<a href="http://www.bbb.org/south-east-florida/business-reviews/financial-services/bankers-healthcare-group-in-southwest-ranches-fl-26001306">
							<img src="<?php echo $assets; ?>images/newsletter/bhg-ablogo.jpg" width="106" height="56"></a>
					</td>

					<td width="250">
						<a href="http://www.inc.com/inc5000/profile/bankers-healthcare-group"><img src="<?php echo $assets; ?>images/newsletter/bhg-inclogo.jpg" width="250" height="56"></a>
					</td>

					<td width="119">
						<a href="http://bhg-inc.com.staging.mindcomet.net/?q=node/add/loan-activation"><img src="<?php echo $assets; ?>images/newsletter/bhg-activate.jpg" width="119" height="56"></a>
					</td>

					<td width="92">
						<a href="http://bhg-inc.com.staging.mindcomet.net/?q=solution/loan-programs"><img src="<?php echo $assets; ?>images/newsletter/bhg-loan.jpg" width="92" height="56"></a>
					</td>

					<td width="91">
						<a href="http://bhg-inc.com.staging.mindcomet.net/?q=support"><img src="<?php echo $assets; ?>images/newsletter/bhg-contact.jpg" width="91" height="56"></a>
					</td>
				</tr>
				
			</table>
					
			<table>		
				<tr>
					<td width="700" align="center" colspan="3"><br><center><span style="font-family: Arial, Helvetica, sans-serif; color: #7a7a7a; font-size: 11px; line-height: 14px; text-decoration: none;">This message was intended for <a href="#" style="font-family: Arial, Helvetica, sans-serif; color: #005696; font-size: 11px; line-height: 14px; text-decoration: underline;"><span style="font-family: Arial, Helvetica, sans-serif; color: #005696; font-size: 11px; line-height: 14px; text-decoration: underline;"><?php echo $recipient; ?></span></a><br />You were added to the system <?php echo date('F j, Y'); ?><br />For more information <a href="#" style="font-family: Arial, Helvetica, sans-serif; color: #005696; font-size: 11px; line-height: 14px; text-decoration: underline;">click here</a></span></center><br></td>
				</tr>	
			</table>
			
			</td>
	</tr>
</table>
</center>
</div>
</body>
</html>
<?php } else { ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css" media="screen">
.ReadMsgBody { width: 100%; }
.ExternalClass { width: 100%; display: block !important; }
#mpf0_MsgContainer{ display: block !important; }
v\:* { behavior: url(#default#VML); display:inline-block; }
body { width:100% !important; font-family:tahoma, arial, helvetica, sans-serif; font-size:11px; color:#474747; background-color:#ffffff; }
table td { border-collapse: collapse; }
a { font-family:tahoma, arial, helvetica, sans-serif; text-decoration:none; font-size:11px; color:#ffffff; }
</style>
</head>

<body topmargin="0" style="margin-top: 0px;" bgcolor="#ffffff">
<div align="center">
<center>
<table width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
		<tr>
			<td valign="top" align="center">
			<table width="700" bgcolor="#ffffff" cellspacing="0" cellpadding="0" align="center">
				
				<tr>
					<td width="700" align="center" colspan="3"><br><center><span style="font-family: Arial, Helvetica, sans-serif; color: #7a7a7a; font-size: 11px; line-height: 14px; text-decoration: none;">Note: To ensure delivery to your inbox, please add <a href="mailto:info@bhg-inc.com" style="font-family: Arial, Helvetica, sans-serif; color: #005696; font-size: 11px; line-height: 14px; text-decoration: underline;"><span style="font-family: Arial, Helvetica, sans-serif; color: #005696; font-size: 11px; line-height: 14px; text-decoration: underline;">info@bhg-inc.com</span></a> to your address book.</span></center><br></td>
				</tr>
			</table>
			
			<table style="border: 1px solid #e3e3e3;" width="660" bgcolor="#ffffff" cellspacing="0" cellpadding="0" align="center">
				<tr height="65">
					<td width="700" colspan="3">
						<a href=""><img src="<?php echo $assets; ?>images/newsletter/bhg_header.jpg" width="660" height="65" /></a>
					</td>
					
					
				</tr>
				
				<tr>
					<td width="660">
						<img src="<?php echo $assets; ?>images/newsletter/bhg_image.jpg" width="660" height="100" />
					</td>
				</tr>
				
				<tr>
					
					
					<td width="660" bgcolor="#ffffff">
						<p style="font-family: Lucida Grande, sans-serif; color: #7a7a7a; font-size: 30px; font-style: italic; text-decoration: none; margin: 0; padding: 0 0 0 20px;">New Loan Activation Submission</p>
						<p style="font-family: Lucida Grande, sans-serif; color: #7a7a7a; font-size: 13px; line-height: 20px; text-decoration: none; margin: 0; padding: 20px;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam nibh. Nunc varius facilisis eros. Sed erat. In in velit quis arcu ornare laoreet. Curabitur adipiscing luctus massa. Integer ut purus ac augue commodo commodo. Nunc nec mi eu justo tempor consectetuer. Etiam vitae nisl. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam nibh. Nunc varius facilisis eros. </p>
					</td>
			</table>		
					
			<table>		
				<tr>
					<td width="700" align="center" colspan="3"><br><center><span style="font-family: Arial, Helvetica, sans-serif; color: #7a7a7a; font-size: 11px; line-height: 14px; text-decoration: none;">This message was intended for <a href="#" style="font-family: Arial, Helvetica, sans-serif; color: #005696; font-size: 11px; line-height: 14px; text-decoration: underline;"><span style="font-family: Arial, Helvetica, sans-serif; color: #005696; font-size: 11px; line-height: 14px; text-decoration: underline;"><?php echo $recipient; ?></span></a><br />You were added to the system September 24, 2007.<br />For more information <a href="#" style="font-family: Arial, Helvetica, sans-serif; color: #005696; font-size: 11px; line-height: 14px; text-decoration: underline;">click here</a></span></center><br></td>
				</tr>	
			</table>
			
			</td>
	</tr>
</table>
</center>
</div>
</body>
</html>
<?php } ?>
