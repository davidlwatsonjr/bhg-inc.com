<?php

/**
 * @file
 * Default theme implementation to format an HTML mail.
 *
 * Copy this file in your default theme folder to create a custom themed mail.
 * Rename it to mimemail-message--[mailkey].tpl.php to override it for a
 * specific mail.
 *
 * Available variables:
 * - $subject: The message subject.
 * - $body: The message body in HTML format.
 * - $mailkey: The message identifier.
 * - $recipient: An email address or user object who is receiving the message.
 * - $css: Internal style sheets.
 * - $assets: Fully-qualified path to images and stylesheets
 *
 * @see template_preprocess_mimemail_message()
 */

 $assets = "http://bhg-inc.com.staging.mindcomet.net/sites/all/themes/bhg/";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css" media="screen">
.ReadMsgBody { width: 100%; }
.ExternalClass { width: 100%; display: block !important; }
#mpf0_MsgContainer{ display: block !important; }
v\:* { behavior: url(#default#VML); display:inline-block; }
body { width:100% !important; font-family:tahoma, arial, helvetica, sans-serif; font-size:11px; color:#474747; background-color:#ffffff; }
table td { border-collapse: collapse; }
a { font-family:tahoma, arial, helvetica, sans-serif; text-decoration:none; font-size:11px; color:#ffffff; }
</style>
</head>

<body topmargin="0" style="margin-top: 0px;" bgcolor="#ffffff">
<div align="center">
<center>
<table width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
		<tr>
			<td valign="top" align="center">
			<table width="700" bgcolor="#ffffff" cellspacing="0" cellpadding="0" align="center">
				
				<tr>
					<td width="700" align="center" colspan="3"><br><center><span style="font-family: Arial, Helvetica, sans-serif; color: #7a7a7a; font-size: 11px; line-height: 14px; text-decoration: none;">Note: To ensure delivery to your inbox, please add <a href="mailto:info@bhg-inc.com" style="font-family: Arial, Helvetica, sans-serif; color: #005696; font-size: 11px; line-height: 14px; text-decoration: underline;"><span style="font-family: Arial, Helvetica, sans-serif; color: #005696; font-size: 11px; line-height: 14px; text-decoration: underline;">info@bhg-inc.com</span></a> to your address book.</span></center><br></td>
				</tr>
			</table>
			
			<table style="border: 1px solid #e3e3e3;" width="660" bgcolor="#ffffff" cellspacing="0" cellpadding="0" align="center">
				<tr height="65">
					<td width="700" colspan="3">
						<a href=""><img src="<?php echo $assets; ?>images/bhg_header.jpg" width="660" height="65" /></a>
					</td>
					
					
				</tr>
				
				<tr>
					<td width="660">
						<img src="<?php echo $assets; ?>images/bhg_image.jpg" width="660" height="100" />
					</td>
				</tr>
				
				<tr>
					
					
					<td width="660" bgcolor="#ffffff">
						<p style="font-family: Lucida Grande, sans-serif; color: #7a7a7a; font-size: 30px; font-style: italic; text-decoration: none; margin: 0; padding: 0 0 0 20px;">New Loan Activation Submission</p>
						<p style="font-family: 'Lucida Grande', sans-serif; color: #7a7a7a; font-size: 13px; line-height: 20px; text-decoration: none; margin: 0; padding: 20px;"><?php echo $body; ?></p>
					</td>
			</table>		
					
			<table>		
				<tr>
					<td width="700" align="center" colspan="3"><br><center><span style="font-family: Arial, Helvetica, sans-serif; color: #7a7a7a; font-size: 11px; line-height: 14px; text-decoration: none;">This message was intended for <a href="#" style="font-family: Arial, Helvetica, sans-serif; color: #005696; font-size: 11px; line-height: 14px; text-decoration: underline;"><span style="font-family: Arial, Helvetica, sans-serif; color: #005696; font-size: 11px; line-height: 14px; text-decoration: underline;"><?php echo $recipient; ?></span></a><br />You were added to the system <?php echo date('F j, Y'); ?><br />For more information <a href="#" style="font-family: Arial, Helvetica, sans-serif; color: #005696; font-size: 11px; line-height: 14px; text-decoration: underline;">click here</a></span></center><br></td>
				</tr>	
			</table>
			
			</td>
	</tr>
</table>
</center>
</div>
</body>
</html>
