<?php
/**
* Add / modify variables before the page renders.
*/
// function phptemplate_preprocess_page(&$vars) {
//  
// // custom content type page template
//   // Renders a new page template to the list of templates used if it exists
//   if (isset($vars['node'])) {
//     // This code looks for any page-custom_content_type.tpl.php page
//     $vars['template_files'][] = 'page-'. str_replace('_', '-', $vars['node']->type);  
//   }
// }

function bhg_preprocess_page(&$vars) {
	$path = drupal_get_path_alias($_GET['q']);
	$fs_refer = explode("/", $path);
	$links = array(
		'articles' => '<a href="/?q=tips-resources/articles">Articles</a>',
		'bhg_advantage' => '<a href="/?q=bhg-advantage/benefits-advantages">BHG Advantage: </a>',
		'benefits_advantages' => '<a href="/?q=bhg-advantage/benefits-advantages">Benefits &amps; Advantages</a>',
		'company' => '<a href="/?q=company/history-mission">Company: </a>',
		'loan_programs' => '<a href="/?q=solution/loan-programs">Loan Programs</a>',
		'media_room' => '<a href="/?q=company/media-room/news-press">Media Room: </a>',
		'news_press' => '<a href="/?q=company/media-room/news-press">News &amp; Press</a>',
		'newsletters' => '<a href="/?q=company/media-room/newsletters">Newsletters</a>',
		'solutions' => '<a href="/?q=solution/loan-programs">Solutions: </a>',
		'tips_resources' => '<a href="/?q=tips-resources/articles">Tips &amp; Resources: </a>',
		'white_papers' => '<a href="/?q=tips-resources/white-papers">White Papers</a>',
	);

	$is_a_matching_node = true;
	if (isset ($vars['node'])) {
		switch ($vars['node']->type) {
			case 'customer_testimonial': 
				$vars['title'] = 'Customer Testimonial';
				break;
			case 'loan_activation':
				//Any loan activation should have this title set
				$vars['head_title'] = 'Activate a Loan';
				break;
			case 'loan_program': 
				$vars['article_title'] = $vars['title']; 
				$vars['title'] = $links['solutions'].'Loan Programs';
				break;
			case 'newsitem': 
				$vars['article_title'] = $vars['title']; 
				$vars['title'] = $links['company'].$links['media_room'].'News &amp; Press';
				break;
			case 'newsletter': 
				$vars['article_title'] = $vars['title']; 
				$vars['title'] = $links['company'].$links['media_room'].'Newsletters';
				break;
			case 'resource': 
				$vars['article_title'] = $vars['title']; 
				$vars['title'] = $links['tips_resources'];
				if (isset($vars['node']->taxonomy[15])) {
					$vars['title'] .= 'White Papers';
				}
				if (isset($vars['node']->taxonomy[12])) {
					$vars['title'] .= 'Articles';
				}
				break;
			default:
				$is_a_matching_node = false;
		}
	} else {
		$is_a_matching_node = false;
	}
	if (!$is_a_matching_node) {
		if (in_array($fs_refer[0], array('bhg-advantage'))) {
			$vars['title'] = $links['bhg_advantage'].$vars['title'];
		} else if (in_array($fs_refer[0], array('solution', 'solutions'))) {
			$vars['title'] = $links['solutions'].$vars['title'];
		} else if (in_array($fs_refer[0], array('tips-resources'))) {
			$vars['title'] = $links['tips_resources'].$vars['title'];
		} else if (in_array($fs_refer[0], array('company', 'faq'))) {
			if (in_array($fs_refer[1], array('media-room'))) {
				$vars['title'] = $links['media_room'].$vars['title'];
			}
			$vars['title'] = $links['company'].$vars['title'];
		}
	}
}

function bhg_menu_item_link($link) {
	if (empty($link['localized_options'])) {
		$link['localized_options'] = array();
	}
	if (empty($link['localized_options']['attributes'])) {
		$link['localized_options']['attributes'] = array();
	}
	if (empty($link['localized_options']['attributes']['class'])) {
		$link['localized_options']['attributes']['class'] = '';
	}
	$q = explode("/", $_GET['q']);
	$active_menus = array();
	if ('node' === $q[0]) {
		global $_this_node;
		if (!$_this_node) {
			$_this_node = node_load($q[1]);
		}
		switch ($_this_node->type) {
			case 'customer_testimonial': 
				$active_menus[] = '670';
				$active_menus[] = '790';
				$active_menus[] = '611';
				break;
			case 'newsitem':
				$active_menus[] = '667';
				$active_menus[] = '629';
				$active_menus[] = '614';
				break;
			case 'newsletter': 
				$active_menus[] = '668';
				$active_menus[] = '629';
				$active_menus[] = '614';
				break;
			case 'resource':
				//Articles
				if (isset($_this_node->taxonomy[12])) {
					$active_menus[] = '768';
				}
				//Podcasts
				if (isset($_this_node->taxonomy[13])) {
					$active_menus[] = '769';
				}
				//Videos
				if (isset($_this_node->taxonomy[14])) {
					$active_menus[] = '770';
				}
				//White Papers
				if (isset($_this_node->taxonomy[15])) {
					$active_menus[] = '771';
				}
				$active_menus[] = '613';
				break;
			default:
		}
	} else {
		$path = drupal_get_path_alias($_GET['q']);
		$q = explode("/", $path);
		switch($q[0]) {
			case 'bhg-advantage':
				$active_menus[] = '611';
				switch($q[1]) {
					case 'customer-testimonials':
						$active_menus[] = '790';
						break;
					default:
				}
				break;
			case 'solution':
				$active_menus[] = '612';
				break;
			case 'tips-resources':
				$active_menus[] = '613';
				break;
			case 'company':
				$active_menus[] = '614';
				break;
			default:
		}
	}
global $something;
if (!$something) {
//var_export($q);
$something=true;
}
	if (in_array($link['mlid'], $active_menus)) {
		$link['localized_options']['attributes']['class'] .= ' active-trail';
	}
	return l($link['title'], $link['href'], $link['localized_options']);
}

function bhg_preprocess_node(&$variables) {
	switch ($variables['type']) {
	case 'customer_testimonial':
		$variables['title'] = 'Customer Testimonial';
		break;
	}	
}

function bhg_node_form_submit($form, &$form_state) {
  global $user;

  $node = node_form_submit_build_node($form, $form_state);
  $insert = empty($node->nid);
  node_save($node);
  $node_link = l(t('view'), 'node/'. $node->nid);
  $watchdog_args = array('@type' => $node->type, '%title' => $node->title);
  $t_args = array('@type' => node_get_types('name', $node), '%title' => $node->title);

  if ($insert) {
    watchdog('content', '@type: added %title.', $watchdog_args, WATCHDOG_NOTICE, $node_link);
    drupal_set_message(t('@type %title has now has been created.', $t_args));
  }
  else {
    watchdog('content', '@type: updated %title.', $watchdog_args, WATCHDOG_NOTICE, $node_link);
    drupal_set_message(t('@type %title has been updated.', $t_args));
  }
  if ($node->nid) {
    unset($form_state['rebuild']);
    $form_state['nid'] = $node->nid;
    $form_state['redirect'] = 'node/'. $node->nid;
  }
  else {
    // In the unlikely case something went wrong on save, the node will be
    // rebuilt and node form redisplayed the same way as in preview.
    drupal_set_message(t('The post could not be saved.'), 'error');
  }
}

/**
* make views mini pager a jump pager
*/
function bhg_views_mini_pager($tags = array(), $limit = 10, $element = 0, $parameters = array(), $quantity = 9) {
	global $pager_page_array, $pager_total, $pager_total_items;

	// Calculate various markers within this pager piece:
	// Middle is used to "center" pages around the current page.
	$pager_middle = ceil($quantity / 2);
	// current is the page we are currently paged to
	$pager_current = $pager_page_array[$element];
	// max is the maximum page number
	$pager_max = $pager_total[$element];
	// End of marker calculations.


	$li_previous = theme('pager_previous', (isset($tags[1]) ? $tags[1] : t('‹')), $limit, $element, 1, $parameters);
	if (empty($li_previous)) {
	//Commented this out since I don't think we want the previous and next li's showing at all if they're empty
	//This makes it easier to style to the edge... POW!
	//	$li_previous = "&nbsp;";
	}

	$li_next = theme('pager_next', (isset($tags[3]) ? $tags[3] : t('›')), $limit, $element, 1, $parameters);
	if (empty($li_next)) {
	//	$li_next = "&nbsp;";
	}

for ($i = 1; $i <= $pager_max; $i++) {
	$p[] = $i;
}
$select = array(
	'#type' => 'select',
	'#title' => t('Jump to'),
	'#options' => $p,
	'#name' => 'page',
	'#id' => 'pager-select-page',
	'#class' => 'jquery_dropdown',
	'#value' => $pager_current,
);
/* Commenting all this out since none of it needs used anymore
drupal_add_js("
$(document).ready(function() {
	$('.pager .pager-current .jquery_dropdown_list a').mouseup(function() {
		$(this).parents('form').find('select').val($(this).attr('rel'));
		$(this).parents('form').submit();
	});
	$('.pager .pager-current .jquery_dropdown_header').html(".($_GET['page']+1).");
	$('.pager').parents('.item-list').clone().insertAfter($('#content .view-filters'));
});", 'inline');
*/

foreach ($_GET as $k => $g) {
	if ($k == 'page') { continue; }
	$hidden .= '<input type="hidden" name="'.$k.'" value="'.drupal_urlencode($g).'" />';
}

	if ($pager_total[$element] > 1) {
		if ($li_previous) {
			$items[] = array(
				'class' => 'pager-previous', 
				'data' => $li_previous,
			);
		}

		$items[] = array(
			'class' => 'pager-current', 
			'data' => '<form class="pager-form">'.$hidden.theme_select($select).'</form>',//t('@current of @max', array('@current' => $pager_current, '@max' => $pager_max)),
		);

		if ($li_next) {
			$items[] = array(
				'class' => 'pager-next', 
				'data' => $li_next,
			);
		}

		return theme('item_list', $items, NULL, 'ul', array('class' => 'pager clearfix'));

	}
}

function phptemplate_preprocess_media_youtube_default_external(&$variables) {
  $variables['width'] = 645;
  $variables['height'] = 338;
  $variables['url'] .= '&version=3&autohide=1&showinfo=0';
}

function bhg_node_form ($form) {
	switch($form['type']['#value']) {
		case 'loan_activation':
			drupal_set_title('Activate A Loan');
			$form['#submit'][] = 'bhg_form_submit';
			return '<img src="/sites/default/files/geotrust.png" id="geotrust-logo" width="76" height="70" />'.theme_node_form($form);
			break;
		case 'affiliate_partnership':
			drupal_set_title('Request Affiliate Partnership');
			unset($form['field_affiliation_logo']);
			unset($form['field_company_description']);
			unset($form['buttons']['preview']);
			break;
		case 'solution_finder':
			drupal_set_title('Solution Finder');
			break;
	}
	return theme_node_form($form);
}

