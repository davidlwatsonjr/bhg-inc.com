$(document).ready(function() {
	$('#block-solutions_finder-solutions_finder #edit-field-solution-profession-value-wrapper select').change(function() {
		$('a.prof-block-link').removeClass('active');
		$(this).addClass('active');
		$('#block-solutions_finder-solutions_finder').addClass('full')
			.find('.step1')
				.addClass('faded')
				.fadeTo(0, .4)
				.hover(function() {
					$(this).fadeTo(0, 1).find('*:not(select, label)').fadeTo(0, 1);
				}, function() {
					$(this).fadeTo(0, .4).find('*:not(select, label, .selector > span)').fadeTo(0, .4);
				})
			.end()
			.find('.step2')
				.removeClass('faded')
				.show()
				.find('.validated').remove().end()
				.find('select').val('').end()
				.find('.jquery_dropdown_header').html('').end()
			.end()
			.find('.step3')
				.addClass('faded')
				.show();
		$('#block-quicktabs-home_block').hide();
	});
	$('#edit-field-solution-amount-value, #edit-field-solution-timeframe-value, #edit-field-solution-funds-value').change(function() {
		if ($('#edit-field-solution-amount-value').val() != '' && $('#edit-field-solution-timeframe-value').val() != '' && $('#edit-field-solution-funds-value').val() != '') {
			$('#block-solutions_finder-solutions_finder')
				.find('.step2, .step3').toggleClass('faded').end()
				.find('.step2.faded').hover(function() {
					$(this).find('*:not(select, label)').fadeTo(0, 1);
				}, function() {
					$(this).find('*:not(select, label)').fadeTo(0, .4);
				});
		}
	});
// 	$('#block-solutions_finder-solutions_finder #edit-cont-1000').click(function() {
// 		if ($('#block-solutions_finder-solutions_finder #edit-field-solution-amount-value-wrapper select').val() > 0 && $('#block-solutions_finder-solutions_finder #edit-field-solution-timeframe-value-wrapper select').val() > 0 && $('#block-solutions_finder-solutions_finder #edit-field-solution-funds-value-wrapper select').val() > 0) {
// 			$('#block-solutions_finder-solutions_finder').find('.step2, .step3, #edit-cont-1000').toggle();
// 			$('#block-solutions_finder-solutions_finder').find('.form-submit').appendTo($('#block-solutions_finder-solutions_finder').find('#edit-reset'));
// 		}
// 		return false;
// 	});
	$('#block-solutions_finder-solutions_finder #edit-reset-1000').click(function() {
		$('#block-solutions_finder-solutions_finder')
			.removeClass('full')
			.find('.step1').removeClass('faded').end()
			.find('.step2')
				.hide()
				.find('.validated').remove().end()
				.find('select').val('').end()
			.end()
			.find('.step3')
				.hide()
				.find('input').not('.form-reset').val('').end()
				.find('.form-checkbox').removeAttr('checked').end();
		$('#block-quicktabs-home_block').show();
// 		$('#block-solutions_finder-solutions_finder select').html('');
		return false;
	});
	$('#block-solutions_finder-solutions_finder #edit-field-solution-profession-value-wrapper').wrap('<div class="step1">').before($('#block-solutions_finder-solutions_finder > h2')).before('<h3>BHG is the premier source of financing for healthcare professionals nationwide!</h3>');
	$('#block-solutions_finder-solutions_finder').find('#edit-field-solution-amount-value-wrapper').before('<div class=\"step2\">');
	$('#block-solutions_finder-solutions_finder .step2').prepend('<h2>Answer 3 Simple Questions</h2>');
	$('#block-solutions_finder-solutions_finder').find('#top2, #edit-field-solution-amount-value-wrapper, #edit-field-solution-timeframe-value-wrapper, #edit-field-solution-funds-value-wrapper').appendTo('.step2');
	$('#block-solutions_finder-solutions_finder').find('#edit-field-solution-email-0-email-wrapper').before('<div class=\"step3 faded\">').find('label').html('Which email address can we send your information to?');
	$('#block-solutions_finder-solutions_finder').find('#top3, #edit-field-solution-email-0-email-wrapper, .form-submit, #edit-reset').appendTo('.step3');
	$('#block-solutions_finder-solutions_finder').find('#edit-field-solution-newsletter-value-yes-wrapper').parents('.form-item').insertBefore($('#block-solutions_finder-solutions_finder').find('#edit-reset'));
	$('#block-solutions_finder-solutions_finder .step3').prepend('<h2>Your Information is Ready...</h2>')
	//$('#block-solutions_finder-solutions_finder #node-form  #edit-field-solution-email-0-email').after('<div id="check-email">');
	$('#block-solutions_finder-solutions_finder').find('.step2, .step3').hide();
/*
	$('#block-solutions_finder-solutions_finder #node-form  #edit-field-solution-email-0-email').bind('keyup, blur', function() {
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
			$me = $(this),
			email = $me.val();
		$me.siblings('.not-check').remove();
		if (email.length > 0) {
			if (!(email.length > 5 && reg.test(email))) {
				$me.after('<div class="not-check">'); 
			}
		}
	});
*/
});
