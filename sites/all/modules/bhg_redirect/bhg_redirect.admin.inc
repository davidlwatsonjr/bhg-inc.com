<?php
// $Id$

/**
 * Form builder to list and manage redirects.
 *
 * @ingroup forms
 * @see theme_bhg_redirect_list()
 */
function bhg_redirect_list() {
	$redirects = bhg_redirect_get_redirects();
	$form = array('#tree' => TRUE);
	foreach($redirects as $redirect) {
		$form[$redirect->rid]['#redirect'] = $redirect;
		$form[$redirect->rid]['url_key'] = array('#value' => check_plain($redirect->url_key));
		$form[$redirect->rid]['edit'] = array('#value' => l(t('edit'), "admin/user/bhg_redirect/edit/$redirect->rid"));
		$form[$redirect->rid]['redirect_name'] = array('#value' => check_plain($redirect->redirect_name));
		$form[$redirect->rid]['active'] = array('#value' => ($redirect->active == 1) ? 'Yes' : 'No');
		$form[$redirect->rid]['delete'] = array('#value' => l(t('delete'), "admin/user/bhg_redirect/delete/$redirect->rid"));
	}
	return $form;
}

/**
 * Theme the redirect list.
 *
 * @ingroup themeable
 * @see bhg_redirect_list()
 */
function theme_bhg_redirect_list($form) {
  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['url_key'])) {
      $redirect = &$form[$key];
      $row = array();
      $row[] = drupal_render($redirect['url_key']);
      $row[] = drupal_render($redirect['edit']);
      $row[] = drupal_render($redirect['redirect_name']);
      $row[] = drupal_render($redirect['active']);
      $row[] = drupal_render($redirect['delete']);
      $rows[] = array('data' => $row);
    }
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No redirects available.'), 'colspan' => '5'));
  }

  $header = array(t('Redirect URL'), t('Edit Redirect'), t('Redirect Value'), t('Active'), t('Delete'));
  return theme('table', $header, $rows, array('rid' => 'bhg_redirect')) . drupal_render($form);
}

/**
 * Display form for adding and editing redirects.
 *
 * @ingroup forms
 * @see bhg_redirect_form_submit()
 */
function bhg_redirect_form(&$form_state, $edit = array()) {
	$edit += array(
		'url_key' => '',
		'redirect_name' => '',
	);
	$form['redirect'] = array(
	    '#type' => 'fieldset',
	    '#title' => t('Redirect'),
	    '#collapsible' => FALSE,
	  );
	$form['redirect']['url_key'] = array(
		'#title' => t('Url Key'),
		'#type' => 'textfield',
		'#default_value' => $edit['url_key'],
		'#description' => t('The URL key for this redirect entry.'),
		'#maxlength' => 64,
		'#required' => TRUE,
	);
	$form['redirect']['redirect_name'] = array(
		'#title' => t('Name'),
		'#type' => 'textfield',
		'#default_value' => $edit['redirect_name'],
		'#description' => t('The person\'s name.'),
		'#maxlength' => 256,
		'#required' => TRUE,
	);
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Add Redirect'),
	);
	if (isset($edit['rid'])) {
		$form['submit']['#value'] = t('Save');
		$form['delete'] = array('#type' => 'submit', '#value' => t('Delete'));
		$form['rid'] = array('#type' => 'value', '#value' => $edit['rid']);
	}
	return $form;
}

/**
 * Validate redirect entry form.
 */
function bhg_redirect_form_validate($form, &$form_stae) {
	if ($form['redirect']['submit']['#value'] == t('Add Redirect')) {
		//make sure the url_key doesn't already exist
		$sql = "SELECT COUNT(rid) FROM {mc_bhg_redirect} WHERE url_key = '%s'";
		$result = db_result(db_query($sql, $form['redirect']['url_key']['#value']));
		if ($result != 0) {
			form_set_error('url_key', t('The specified url_key already exists.'));
		}
	}
}

/**
 * Process redirect entry form.
 */
function bhg_redirect_form_submit($form, &$form_state) {
	switch (bhg_redirect_save_redirect($form_state['values'])) {
		case SAVED_NEW:
			drupal_set_message(t('Created new redirect %url_key.', array('%url_key' => $form_state['values']['url_key'])));
			watchdog('bhg_redirect', 'Created new redirect %url_key.', array('%name' => $form_state['values']['url_key']), WATCHDOG_NOTICE, l(t('edit'), 'admin/user/bhg_redirect/edit/'. $form_state['values']['rid']));
			break;
		case SAVED_UPDATED:
			drupal_set_message(t('Updated redirect %url_key.', array('%url_key' => $form_state['values']['url_key'])));
			watchdog('bhg_redirect', 'Updated redirect %url_key.', array('%name' => $form_state['values']['url_key']), WATCHDOG_NOTICE, l(t('edit'), 'admin/user/bhg_redirect/edit/'. $form_state['values']['rid']));
			break;
	}

	$form_state['rid'] = $form_state['values']['rid'];
	$form_state['redirect'] = 'admin/user/bhg_redirect';
	return;
}

/**
 * Page to edit a redirect.
 */
function bhg_redirect_edit($redirect) {
  if ((isset($_POST['op']) && $_POST['op'] == t('Delete')) || isset($_POST['confirm'])) {
    return drupal_get_form('bhg_redirect_confirm_delete', $redirect->rid);
  }
  else {
    return drupal_get_form('bhg_redirect_form', (array)$redirect);
  }
}

/**
 * Page to delete a redirect.
 */
function bhg_redirect_delete($rid) {
  return drupal_get_form('bhg_redirect_confirm_delete', $rid);
}

/**
 * Form builder for the redirect delete confirmation form.
 *
 * @ingroup forms
 * @see bhg_redirect_confirm_delete_submit()
 */
function bhg_redirect_confirm_delete(&$form_state, $rid) {
  $redirect = bhg_redirect_load($rid);

  $form['type'] = array('#type' => 'value', '#value' => 'redirect');
  $form['rid'] = array('#type' => 'value', '#value' => $rid);
  $form['url_key'] = array('#type' => 'value', '#value' => $redirect->url_key);
  return confirm_form($form,
                  t('Are you sure you want to delete the redirect for %name?',
                  array('%name' => $redirect->redirect_name)),
                  'admin/user/bhg_redirect/',
                  t('This action cannot be undone.'),
                  t('Delete'),
                  t('Cancel'));
}

/**
 * Submit handler to delete a redirect after confirmation.
 *
 * @see bhg_redirect_confirm_delete()
 */
function bhg_redirect_confirm_delete_submit($form, &$form_state) {
  $status = bhg_redirect_del_redirect($form_state['values']['rid']);
  drupal_set_message(t('Deleted redirect %url_key.', array('%url_key' => $form_state['values']['url_key'])));
  watchdog('bhg_redirect', 'Deleted redirect %url_key.', array('%url_key' => $form_state['values']['url_key']), WATCHDOG_NOTICE);
  $form_state['redirect'] = 'admin/user/bhg_redirect';
  return;
}