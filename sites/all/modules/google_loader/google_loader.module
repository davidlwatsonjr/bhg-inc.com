<?php

/**
 * @file
 * Loads various JavaScript libraries via the Google Loader API, including jQuery.
 */

/**
 * Implementation of hook_flush_caches().
 */
function google_loader_flush_caches() {
  google_loader_jquery_version();
}

/**
 * Implementation of hook_menu().
 */
function google_loader_menu() {
  $items['admin/settings/google-loader'] = array(
    'title' => 'Google Loader',
    'description' => t('Configure Google Loader settings.'),
    'file' => 'google_loader.admin.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('google_loader_admin_settings'),
    'access callback' => 'user_access',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implementation of hook_theme().
 */
function google_loader_theme($existing, $type, $theme, $path) {
  return array(
    'google_loader_loader' => array(
      'arguments' => array('loads' => NULL),
      'template' => 'google_loader-loads',
    ),
  );
}

/**
 * Implementation of hook_theme_registry_alter().
 */
function google_loader_theme_registry_alter(&$theme_registry) {
  if (isset($theme_registry['page'])) {
    if (count($theme_registry['page']['preprocess functions']) > 0) {
      // remove jquery_update preprocess callback if exists
      if ($key = array_search('jquery_update_preprocess_page', $theme_registry['page']['preprocess functions'])) {
        unset($theme_registry['page']['preprocess functions'][$key]);
      }

      // remove google_loader preprocess callback if exists
      if ($key = array_search('google_loader_preprocess_page', $theme_registry['page']['preprocess functions'])) {
        unset($theme_registry['page']['preprocess functions'][$key]);
      }
    }

    // Now tack it on at the end so it runs after everything else.
    $theme_registry['page']['preprocess functions'][] = 'google_loader_preprocess_page';
  }
}

/**
 * Implementation of hook_preprocess_page()
 */
function google_loader_preprocess_page(&$variables) {
  global $language, $theme_info;

  // get the path from the requested URL
  $path = $_REQUEST['q'] ? $_REQUEST['q'] : $_REQUEST['destination'];

  // remove language prefix from path if language negotioation is set to use path prefix
  if (LANGUAGE_NEGOTIATION_PATH_DEFAULT) {
    $path = preg_replace("/^$language->language(\/?)/", '', $path);
  }

  // only do this for pages that have JavaScript on them.
  if (!empty($variables['scripts'])) {
	$theme_default = variable_get('theme_default','none');

	// get jQuery version to load from google
    $jquery_version = variable_get('google_loader_jquery_version', '1.2.6');

	// get jQuery version to load from google for default theme
	$jquery_version_default = variable_get('google_loader_jquery_version_default', '0');

	// get paths to exclude from default theme jQuery version
	$jquery_version_default_exclude = variable_get('google_loader_jquery_version_default_exclude', '');

	// check if jQuery version for default theme is set, is greater than default jQuery version, and the current theme is the default theme
	if (!drupal_match_path($path, $jquery_version_default_exclude) && $jquery_version_default && version_compare($jquery_version_default, $jquery_version, '>') && ($theme_info->name == $theme_default || $theme_info->info['base theme'] == $theme_default))
	{
		// set new version of jQuery to use
		$jquery_version = $jquery_version_default;
	}

    // set default jQuery UI version
    $jquery_ui_version = '1.8';

    // get an array of all the JavaScript files loaded by Drupal on this page
    $scripts = drupal_add_js();

    // remove jQuery core
    unset($scripts['core']['misc/jquery.js']);

    // set default for jQuery UI Load
    $jquery_ui_load = FALSE;

    // check if jQuery UI module exists
    if (module_exists('jquery_ui')) {
      foreach ($scripts['module'] as $file => $settings) {
        if (preg_match('/\/jquery_ui\/|\/jquery.ui\//', $file)) {
          // remove script	
          unset($scripts['module'][$file]);

          // get jQuery UI version from jQuery UI module
          $jquery_ui_version = jquery_ui_get_version();

          // force load jQuery UI from google
          $jquery_ui_load = TRUE;
        }
      }
    }

    // reset themed scripts variable
    $variables['scripts'] = drupal_get_js('header', $scripts);

    $modules = array(array('name' => 'jquery', 'version' => $jquery_version));

    // Chrome Frame (1.x)
    if (variable_get('google_loader_chrome-frame', 0)) {
      $modules[] = array('name' => 'chrome-frame', 'version' => '1');
    }

    // Dojo (1.6.x)
    if (variable_get('google_loader_dojo', 0)) {
      $modules[] = array('name' => 'dojo', 'version' => '1.6');
    }

    // Ext Core (3.1.x)
    if (variable_get('google_loader_ext-core', 0)) {
      $modules[] = array('name' => 'ext-core', 'version' => '3.1');
    }

    // jQuery UI (1.8.x)
    if ($jquery_ui_load || variable_get('google_loader_jqueryui', 0)) {
      $modules[] = array('name' => 'jqueryui', 'version' => $jquery_ui_version);
    }

    // MooTools (1.3.x)
    if (variable_get('google_loader_mootools', 0)) {
      $modules[] = array('name' => 'mootools', 'version' => '1.3');
    }

    // Prototype (1.7.x)
    if (variable_get('google_loader_prototype', 0)) {
      $modules[] = array('name' => 'prototype', 'version' => '1.7');
    }

    // script.aculo.us (1.8.x)
    if (variable_get('google_loader_scriptaculous', 0)) {
      $modules[] = array('name' => 'scriptaculous', 'version' => '1.8');
    }

    // SWFObject (2.x)
    if (variable_get('google_loader_swfobject', 0)) {
      $modules[] = array('name' => 'swfobject', 'version' => '2');
    }

    // Yahoo! User Interface Library (3.3.x)
    if (variable_get('google_loader_yui', 0)) {
      $modules[] = array('name' => 'yui', 'version' => '3.3');
    }

    // WebFont Loader (1.x)
    if (variable_get('google_loader_webfont', 0)) {
      $modules[] = array('name' => 'webfont', 'version' => '1');
    }

    // execute hook_loader_script
    $modules = _google_loader_loader_script($modules);

    // convert to js setings and url encode
    $loads = urlencode(drupal_to_js(array('modules' => $modules)));

    // add google loader script to head, to ensure loading happens prior to document ready
    $variables['head'] .= "\n" . theme('google_loader_loader', $loads);
  }
}

/**
 * Add scripts via hook_loader_script
 */
function _google_loader_loader_script($modules) {
  // set hook
  $hook = 'loader_script';

  // iterate through each hook implementation
  foreach (module_implements($hook) as $module) {
    // define function name
    $function = $module . '_' . $hook;

    // get settings from hook
    $function($modules);
  }

  return $modules;
}

/**
 * Get jQuery version
 */
function google_loader_jquery_version($jquery_path = NULL) {
  $jquery_version = 0;

  // use jQuery Update function if it exists
  if (function_exists('jquery_update_get_version')) {
    $jquery_version = jquery_update_get_version();
  }
  else {
  // create regEx pattern for determining jQuery version from file
    $pattern = '# * jQuery ([0-9\.a-z]+) - New Wave Javascript#';

    // default to core jQuery path if no jQuery path is provided
    if (is_null($jquery_path)) {
      $jquery_path = 'misc/jquery.js';
    }

    // get contents of jQuery file
    $jquery = file_get_contents($jquery_path);

    // get version from file contents
    if (preg_match($pattern, $jquery, $matches)) {
      $jquery_version = $matches[1];
    }
  }

  // check if jQuery version is less than 1.2.6 (default version in Drupal 6).
  if (version_compare($jquery_version, '1.2.6', '<')) {
    $jquery_version = '1.2.6';
  }

  // set version to load from Google
  variable_set('google_loader_jquery_version', $jquery_version);

  return $jquery_version;
}