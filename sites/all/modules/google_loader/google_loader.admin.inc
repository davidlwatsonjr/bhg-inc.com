<?php

/**
 * @file
 * Admin settings page for module.
 */

/**
 * Settings page for module.
 */
function google_loader_admin_settings() {
  // set default jQuery UI version
  $jquery_ui_version = '1.8.x';

  // check if jQuery UI module exists
  if (module_exists('jquery_ui')) {
    // get jQuery UI version from jQuery UI module
    $jquery_ui_version = jquery_ui_get_version();

    // force load jQuery UI from google
    $jquery_ui_load = TRUE;
  }

  $form['libraries'] = array(
    '#type' => 'fieldset',
    '#title' => t('Libraries to Load'),
    '#collapsible' => FALSE
  );

  $form['libraries']['google_loader_chrome-frame'] = array(
    '#type' => 'checkbox',
    '#title' => 'Chrome Frame (1.x)',
    '#default_value' => variable_get('google_loader_chrome-frame', NULL),
  );

  $form['libraries']['google_loader_dojo'] = array(
    '#type' => 'checkbox',
    '#title' => 'Dojo (1.6.x)',
    '#default_value' => variable_get('google_loader_dojo', NULL),
  );

  $form['libraries']['google_loader_ext-core'] = array(
    '#type' => 'checkbox',
    '#title' => 'Ext Core (3.1.x)',
    '#default_value' => variable_get('google_loader_ext-core', NULL),
  );

  $form['libraries']['google_loader_jquery'] = array(
    '#type' => 'checkbox',
    '#title' => 'jQuery (' . variable_get('google_loader_jquery_version', '1.2.6') . ')',
    '#default_value' => 1,
    '#disabled' => TRUE
  );

  $form['libraries']['google_loader_jqueryui'] = array(
    '#type' => 'checkbox',
    '#title' => 'jQuery UI (' . $jquery_ui_version . ')',
    '#default_value' => $jquery_ui_load || variable_get('google_loader_jqueryui', NULL),
    '#disabled' => $jquery_ui_load
  );

  $form['libraries']['google_loader_mootools'] = array(
    '#type' => 'checkbox',
    '#title' => 'MooTools (1.3.x)',
    '#default_value' => variable_get('google_loader_mootools', NULL),
  );

  $form['libraries']['google_loader_prototype'] = array(
    '#type' => 'checkbox',
    '#title' => 'Prototype (1.7.x)',
    '#default_value' => variable_get('google_loader_prototype', NULL),
  );

  $form['libraries']['google_loader_scriptaculous'] = array(
    '#type' => 'checkbox',
    '#title' => 'script.aculo.us (1.8.x)',
    '#default_value' => variable_get('google_loader_scriptaculous', NULL),
  );

  $form['libraries']['google_loader_swfobject'] = array(
    '#type' => 'checkbox',
    '#title' => 'SWFObject (2.x)',
    '#default_value' => variable_get('google_loader_swfobject', NULL),
  );

  $form['libraries']['google_loader_yui'] = array(
    '#type' => 'checkbox',
    '#title' => 'Yahoo! User Interface Library (3.3.x)',
    '#default_value' => variable_get('google_loader_yui', NULL),
  );

  $form['libraries']['google_loader_webfont'] = array(
    '#type' => 'checkbox',
    '#title' => 'WebFont Loader (1.x)',
    '#default_value' => variable_get('google_loader_webfont', NULL),
  );

  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced Settings'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE
  );

  $form['advanced']['google_loader_jquery_version_default'] = array(
    '#type' => 'textfield',
    '#title' => t('jQuery version to load for default theme.'),
    '#description' => t('Version number must be available via Google Loader and must be greater than that of the default jQuery version.'),
    '#default_value' => variable_get('google_loader_jquery_version_default', NULL),
  );

  $form['advanced']['google_loader_jquery_version_default_exclude'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages to exclude from using above version.'),
    '#description' => t('Enter one page per line as Drupal paths. The "*" character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.', array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
    '#default_value' => variable_get('google_loader_jquery_version_default_exclude', NULL),
  );

  return system_settings_form($form);
}
